package com.trivago.personalization.casestudy;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * The type Casestudy application tests.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CasestudyApplicationTests {

    /**
     * Context loads.
     */
    @Test
	public void contextLoads() {
	}

}
