package com.trivago.personalization.casestudy.controller;

import com.trivago.personalization.casestudy.dto.ClicksRsDTO;
import com.trivago.personalization.casestudy.dto.InsightRsDTO;
import com.trivago.personalization.casestudy.dto.Selections;
import com.trivago.personalization.casestudy.dto.SelectionsRsDTO;
import com.trivago.personalization.casestudy.service.Personalization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

/**
 * The type Api controller.
 */
@EnableSwagger2
@RestController
@RequestMapping("/api/v1")
public class ApiController {

    @Autowired
    private Personalization personalization;

    /**
     * Gets top amenities selected by user.
     *
     * @param userId the user id
     * @return the top amenities selected by user
     */
    @GetMapping(path = "/amenities/users/{userId}")
    public List<SelectionsRsDTO> getTopAmenitiesSelectedByUser(
            @PathVariable final Long userId
    ) {
        return personalization.getTopAmenitiesSelectedByUser(userId);
    }

    /**
     * Create amenities selections.
     *
     * @param selections the selections
     * @return the selections
     */
    @PostMapping(path = "/amenities")
    public Selections CreateAmenities(
            @RequestBody final Selections selections
    ) {
        return personalization.createAmenities(selections);
    }

    /**
     * Gets tophotels clicked by user.
     *
     * @param userId the user id
     * @return the tophotels clicked by user
     */
    @GetMapping(path = "/hotels/users/{userId}")
    public List<ClicksRsDTO> getTophotelsClickedByUser(
            @PathVariable final Long userId
    ) {

        return personalization.getMostClickedHotelByUser(userId);
    }

    /**
     * Gets all insight by user.
     *
     * @param userId the user id
     * @return the all insight by user
     */
    @GetMapping(path = "/insights/users/{userId}")
    public InsightRsDTO getAllInsightByUser(
            @PathVariable final Long userId
    ) {

        return personalization.getAllInsightsByUser(userId);
    }


}
