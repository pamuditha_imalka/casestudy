package com.trivago.personalization.casestudy.dto;

/**
 * The type Clicks rs dto.
 */
public class ClicksRsDTO {

    private int numberOfClicks;
    private int hotelId;

    /**
     * Gets number of clicks.
     *
     * @return the number of clicks
     */
    public int getNumberOfClicks() {
        return numberOfClicks;
    }

    /**
     * Sets number of clicks.
     *
     * @param numberOfClicks the number of clicks
     */
    public void setNumberOfClicks(final int numberOfClicks) {
        this.numberOfClicks = numberOfClicks;
    }

    /**
     * Gets hotel id.
     *
     * @return the hotel id
     */
    public int getHotelId() {
        return hotelId;
    }

    /**
     * Sets hotel id.
     *
     * @param hotelId the hotel id
     */
    public void setHotelId(final int hotelId) {
        this.hotelId = hotelId;
    }

}
