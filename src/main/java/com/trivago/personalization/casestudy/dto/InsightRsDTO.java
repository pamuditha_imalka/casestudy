package com.trivago.personalization.casestudy.dto;

import java.util.List;

/**
 * The type Insight rs dto.
 */
public class InsightRsDTO {
    private List<ClicksRsDTO> hotelInsights;

    private List<SelectionsRsDTO> amenitieInsights;

    /**
     * Gets hotel insights.
     *
     * @return the hotel insights
     */
    public List<ClicksRsDTO> getHotelInsights() {
        return hotelInsights;
    }

    /**
     * Sets hotel insights.
     *
     * @param hotelInsights the hotel insights
     */
    public void setHotelInsights(List<ClicksRsDTO> hotelInsights) {
        this.hotelInsights = hotelInsights;
    }

    /**
     * Gets amenitie insights.
     *
     * @return the amenitie insights
     */
    public List<SelectionsRsDTO> getAmenitieInsights() {
        return amenitieInsights;
    }

    /**
     * Sets amenitie insights.
     *
     * @param amenitieInsights the amenitie insights
     */
    public void setAmenitieInsights(List<SelectionsRsDTO> amenitieInsights) {
        this.amenitieInsights = amenitieInsights;
    }

}
