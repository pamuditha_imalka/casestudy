package com.trivago.personalization.casestudy.dto;

/**
 * The type Clicks.
 */
public class Clicks {

    private int timestamp;
    private Long userId;
    private int hotelId;
    private int hotelRegion;

    /**
     * Gets timestamp.
     *
     * @return the timestamp
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * Sets timestamp.
     *
     * @param timestamp the timestamp
     */
    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Gets hotel id.
     *
     * @return the hotel id
     */
    public int getHotelId() {
        return hotelId;
    }

    /**
     * Sets hotel id.
     *
     * @param hotelId the hotel id
     */
    public void setHotelId(int hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * Gets hotel region.
     *
     * @return the hotel region
     */
    public int getHotelRegion() {
        return hotelRegion;
    }

    /**
     * Sets hotel region.
     *
     * @param hotelRegion the hotel region
     */
    public void setHotelRegion(int hotelRegion) {
        this.hotelRegion = hotelRegion;
    }
}
