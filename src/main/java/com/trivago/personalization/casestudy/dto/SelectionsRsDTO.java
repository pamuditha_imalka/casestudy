package com.trivago.personalization.casestudy.dto;

/**
 * The type Selections rs dto.
 */
public class SelectionsRsDTO {
    private int amenityId;
    private int selectedCount;

    /**
     * Gets amenity id.
     *
     * @return the amenity id
     */
    public int getAmenityId() {
        return amenityId;
    }

    /**
     * Sets amenity id.
     *
     * @param amenityId the amenity id
     */
    public void setAmenityId(final int amenityId) {
        this.amenityId = amenityId;
    }

    /**
     * Gets selected count.
     *
     * @return the selected count
     */
    public int getSelectedCount() {
        return selectedCount;
    }

    /**
     * Sets selected count.
     *
     * @param selectedCount the selected count
     */
    public void setSelectedCount(final int selectedCount) {
        this.selectedCount = selectedCount;
    }
}
