package com.trivago.personalization.casestudy.dto;

import java.util.Collection;

/**
 * The type Selections.
 */
public class Selections {

    private int timestamp;
    private Long userId;
    private int amenityId;

    /**
     * Gets timestamp.
     *
     * @return the timestamp
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * Sets timestamp.
     *
     * @param timestamp the timestamp
     */
    public void setTimestamp(final int timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Gets amenity id.
     *
     * @return the amenity id
     */
    public int getAmenityId() {
        return amenityId;
    }

    /**
     * Sets amenity id.
     *
     * @param amenityId the amenity id
     */
    public void setAmenityId(final int amenityId) {
        this.amenityId = amenityId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(final Long userId) {
        this.userId = userId;
    }

}
