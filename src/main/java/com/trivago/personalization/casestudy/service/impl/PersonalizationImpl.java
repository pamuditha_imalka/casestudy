package com.trivago.personalization.casestudy.service.impl;

import com.trivago.personalization.casestudy.dao.PersonalizationRepository;
import com.trivago.personalization.casestudy.dto.*;
import com.trivago.personalization.casestudy.service.Personalization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.aggregation.Aggregation;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * The type Personalization.
 */
@Service
public class PersonalizationImpl implements Personalization {
    /**
     * The Personalization repository.
     */
    @Autowired
    PersonalizationRepository personalizationRepository;

    /**
     * The Mongo template.
     */
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public List<SelectionsRsDTO> getTopAmenitiesSelectedByUser(final Long userId) {

        final Aggregation aggregradedQuary = Aggregation.newAggregation(
                match(Criteria.where("userId").is(userId)),
                group("amenityId").count( ).as("selectedCount"),
                project("selectedCount").and("amenityId").previousOperation(),
                sort(Sort.Direction.DESC, "selectedCount")
        );
        //Convert the aggregation result into a List
        final AggregationResults<SelectionsRsDTO> groupResults
                = mongoTemplate.aggregate(aggregradedQuary, Selections.class, SelectionsRsDTO.class);
        return groupResults.getMappedResults( );
    }

    @Override
    public List<ClicksRsDTO> getMostClickedHotelByUser(final Long userId) {

        final Aggregation aggregradedQuaryHotel = Aggregation.newAggregation(

                match(Criteria.where("userId").is(userId)),
                group("hotelId").count().as("numberOfClicks"),
                project( "numberOfClicks").and("hotelId").previousOperation(),
                sort(Sort.Direction.DESC, "numberOfClicks")
        );
        //Convert the aggregation result into a List
        final AggregationResults<ClicksRsDTO> groupResults
                = mongoTemplate.aggregate(aggregradedQuaryHotel, Clicks.class, ClicksRsDTO.class);
        return groupResults.getMappedResults( );

    }

    public Selections createAmenities(final Selections selections) {
        return personalizationRepository.save(selections);
    }

    @Override
    public InsightRsDTO getAllInsightsByUser(final Long userId) {

        List<ClicksRsDTO> hotelInsights = getMostClickedHotelByUser(userId);
        List<SelectionsRsDTO> amenitieInsights = getTopAmenitiesSelectedByUser(userId);
        InsightRsDTO insightRsDTO = new InsightRsDTO( );
        insightRsDTO.setHotelInsights(hotelInsights);
        insightRsDTO.setAmenitieInsights(amenitieInsights);
        return insightRsDTO;
    }
}
