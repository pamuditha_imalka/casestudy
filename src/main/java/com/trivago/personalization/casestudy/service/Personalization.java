package com.trivago.personalization.casestudy.service;

import com.trivago.personalization.casestudy.dto.ClicksRsDTO;
import com.trivago.personalization.casestudy.dto.InsightRsDTO;
import com.trivago.personalization.casestudy.dto.Selections;
import com.trivago.personalization.casestudy.dto.SelectionsRsDTO;

import java.util.List;

/**
 * The interface Personalization.
 */
public interface Personalization {

    /**
     * Gets top amenities selected by user.
     *
     * @param userId the user id
     * @return the top amenities selected by user
     */
    List<SelectionsRsDTO> getTopAmenitiesSelectedByUser(final Long userId);

    /**
     * Create amenities selections.
     *
     * @param selections the selections
     * @return the selections
     */
    Selections createAmenities(final Selections selections);

    /**
     * Gets most clicked hotel by user.
     *
     * @param userId the user id
     * @return the most clicked hotel by user
     */
    List<ClicksRsDTO> getMostClickedHotelByUser(final Long userId);

    /**
     * Gets all insights by user.
     *
     * @param userId the user id
     * @return the all insights by user
     */
    InsightRsDTO getAllInsightsByUser(final Long userId);
}
