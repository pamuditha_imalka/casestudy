package com.trivago.personalization.casestudy.dao;

import com.trivago.personalization.casestudy.dto.Selections;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * The interface Personalization repository.
 */
public interface PersonalizationRepository extends MongoRepository <Selections, Long>{

    /**
     * Find by user id list.
     *
     * @param userId the user id
     * @return the list
     */
    List<Selections> findByUserId(final Long userId);

}
