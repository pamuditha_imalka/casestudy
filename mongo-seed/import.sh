#! /bin/bash

mongoimport -d trivago -c clicks --type csv --file clicks.csv --headerline

mongoimport -d trivago -c selections --type csv --file selections.csv --headerline